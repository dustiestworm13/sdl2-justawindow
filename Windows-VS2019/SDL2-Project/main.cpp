/*
    Getting started with SDL2 - Just a Window. 
*/

//Include SDL library
#include "SDL.h"
#include "SDL_image.h"
//For exit()
#include <stdlib.h>

int main( int argc, char* args[] )
{
    // Declare window and renderer objects
    SDL_Window* gameWindow = nullptr;
    SDL_Renderer* gameRenderer = nullptr;
    // Temporary surface used while loading the image
    SDL_Surface* temp = nullptr;
    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture* backgroundTexture = nullptr;
    SDL_Texture* playerTexture = nullptr;


    // SDL allows us to choose which SDL componets are going to be
    // initialised. We'll go for everything for now!
    SDL_Init(SDL_INIT_EVERYTHING);

    gameWindow = SDL_CreateWindow("Hello CIS4008",          // Window title
                              SDL_WINDOWPOS_UNDEFINED,  // X position
                              SDL_WINDOWPOS_UNDEFINED,  // Y position
                              800, 600,                 // width, height
                              SDL_WINDOW_SHOWN);        // Window flags

    // if the window creation succeeded create our renderer
    gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

    
  

    //Setup sprite
    temp = IMG_Load("D:/uni work/game sys fund/sdl2-justawindow/Windows-VS2019/SDL2-Project/assets/images/Assets-Week1/Assets-Week1/background.png");
    backgroundTexture =
        SDL_CreateTextureFromSurface(gameRenderer, temp);
    
   
    // Create a texture object from the loaded image
// - we need the renderer we are going to use to draw
// this as well!
// - this provides information about the target format
// to aid optimisation.
    
    // Clean-up - we re done with our temporary image
    // now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;

    temp = IMG_Load("D:/uni work/game sys fund/sdl2-justawindow/Windows-VS2019/SDL2-Project/assets/images/Assets-Week1/Assets-Week1/dorf2.png");
    playerTexture =
        SDL_CreateTextureFromSurface(gameRenderer, temp);
    SDL_FreeSurface(temp);
    temp = nullptr;

    // Player Image - source rectangle
// - The part of the texture to render.
    SDL_Rect sourceRectangle;
    // - Where to render the texture
    SDL_Rect targetRectangle;

    //Setup source and destination rects
    sourceRectangle.x = 0;
    sourceRectangle.y = 0;
    // Use the texture info to get the to get the
    // rectangle size.
    // We are using the whole of the sprite image!
    SDL_QueryTexture(playerTexture, 0, 0,
        &(sourceRectangle.w), //width
        &(sourceRectangle.h)); //height

    // I worked this out via trial and error.
    targetRectangle.x = 100;
    targetRectangle.y = 400;
    // We could choose any size/scale - experiment with
    // this :-D
    targetRectangle.w = sourceRectangle.w * 1.0f;
    targetRectangle.h = sourceRectangle.h * 1.0f;


    //Draw stuff here.
    //  2. Draw the scene
    

   

    // 1. Clear the screen
    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
    

    SDL_RenderClear(gameRenderer);
    SDL_RenderCopy(gameRenderer,
        backgroundTexture,
        NULL, NULL);
    SDL_RenderCopy(gameRenderer, // where to render
        playerTexture, // what to render
        &sourceRectangle, // source
        &targetRectangle); // destination
    // 2. Draw the game objects
    


    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

    //Pause to allow the image to be seen
    SDL_Delay( 10000 );

    //Clean up!
    SDL_DestroyTexture(playerTexture);
    SDL_DestroyTexture(backgroundTexture);
    SDL_DestroyRenderer(gameRenderer);
    SDL_DestroyWindow(gameWindow);
    // Clean-up - we are done with "temp" now
    


    //Shutdown SDL - clear up resources etc. 
    SDL_Quit();

    exit(0);
}